'use strict';

module.exports = function(app) {
	var statuses = require('../../app/controllers/statuses.server.controller');
	var users = require('../../app/controllers/users.server.controller');

	app.route('/statuses')
		.get(users.requiresLogin, statuses.list)
		.post(users.requiresLogin, statuses.create);

	app.route('/statuses/:statusId')
    .get(users.requiresLogin, statuses.read)
    .put(users.requiresLogin, statuses.update);

	// Finish by binding the article middleware
	// What's this? Where the categoryId is present in the URL
	// the logic to 'get by id' is handled by this single function
	// and added to the request object i.e. request.category.

	//app.param('statusId', statuses.statusByID);

};