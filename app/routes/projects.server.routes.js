'use strict';

module.exports = function(app) {
	var projects = require('../../app/controllers/projects.server.controller');
	var users = require('../../app/controllers/users.server.controller');

	app.route('/projects')
		.get(projects.list)
		.post(users.requiresLogin, projects.create);

      // the projectId param is added to the params object for the request
    app.route('/projects/:projectId')
        .get(projects.read)
        .put(users.requiresLogin, projects.update)
        .delete(users.requiresLogin, projects.delete);

        //app.param('projectId', projects.projectByID);

};
