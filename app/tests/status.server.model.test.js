'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    Status = mongoose.model('Status');

/**
 * Unit tests
 */
describe('Status Model', function() {

    describe('Saving', function() {
        it('saves new record', function(done) {
            var status = new Status({
                name: 'Beverages',
                description: 'Soft drinks, coffees, teas, beers, and ales'
            });

            status.save(function(err, saved) {
                should.not.exist(err);
                done();
            });
        });

        it('throws validation error when name is empty', function(done) {
            var status = new Status({
                description: 'Soft drinks, coffees, teas, beers, and ales'
            });

            status.save(function(err) {
                should.exist(err);
                err.errors.name.message.should.equal('name cannot be blank');
                done();
            });
        });

        it('throws validation error when name longer than 15 chars', function(done) {
            var status = new Status({
                name: 'Grains/Cereals/Chocolates'
            });

            status.save(function(err, saved) {
                should.exist(err);
                err.errors.name.message.should.equal('name must be 15 chars in length or less');
                done();
            });
        });

        it('throws validation error for duplicate status name', function(done) {
            var status = new Status({
                name: 'Beverages'
            });

            status.save(function(err) {
                should.not.exist(err);

                var duplicate = new Status({
                    name: 'Beverages'
                });

                duplicate.save(function(err) {
                    err.err.indexOf('$name').should.not.equal(-1);
                    err.err.indexOf('duplicate key error').should.not.equal(-1);
                    should.exist(err);
                    done();
                });
            });
        });
    });

    afterEach(function(done) {
        // NB this deletes ALL categories (but is run against a test database)
        Status.remove().exec();
        done();
    });
});