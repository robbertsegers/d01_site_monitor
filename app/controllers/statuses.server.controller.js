'use strict';

/**  Module dependencies.  */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
	Status = mongoose.model('Status'),
    _ = require('lodash');

/**  Create a status  */
exports.create = function(req, res) {
    var status = new Status(req.body);
    console.log(req.body);

    status.save(function(err) {
        if (err) {
            console.log(err);
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.status(201).json(status);
        }
    });
};

/**  Show the current Status  */
exports.read = function(req, res) {
    Status.findById(req.params.categoryId).exec(function(err, status) {
        if (err) {
          return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
          });
      } else {
         if (!status) {
                return res.status(404).send({
                    message: 'Status not found'
                });
            }
            res.json(status);
      }
    });
};

/**  Update a Status  */
exports.update = function(req, res) {
	var status = req.status;

	status = _.extend(status, req.body);

	status.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(status);
		}
	});
};

/**  Delete an Status  */
exports.delete = function(req, res) {

};

/**  List of Statuses  */
exports.list = function(req, res) {
    Status.find().exec(function(err, statuses) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(statuses);
        }
    });
};