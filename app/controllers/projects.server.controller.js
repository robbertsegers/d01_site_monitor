'use strict';

var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    Project = mongoose.model('Project'),
    Omgeving = mongoose.model('Omgeving'), // added omgeving model
    _ = require('lodash');

/**  Create a Project*/
exports.create = function(req, res) {
    console.log(req.body);
    var project = new Project(req.body);
    var status = new Omgeving();
    console.log(project);

    project.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.status(201).json(project);
            console.log(project);
        }
    });
};

/**  Show the current Project  */
exports.read = function(req, res) {
    Project.findById(req.params.projectId).exec(function(err, project) {
        if (err) {
          return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
          });
      } else {
         if (!project) {
                return res.status(404).send({
                    message: 'Project not found'
                });
            }
            res.json(project);
      }
    });
};

/**  Update a project  */
exports.update = function(req, res) {
    Project.update({_id: req.body._id}, req.body, function(err, update) {
    //todo.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(req.body);
        }
    });
};

/**  Delete an project  */
exports.delete = function(req, res) {
    Project.remove({
        _id: req.params.projectId
    }, function(error, project) {
        if (error) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(error)
            });
        } else {
            res.status(204).send({});
        }
    });
};

/**  List of projects  */
exports.list = function(req, res) {
    Project.find().exec(function(err, projects) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(projects);
        }
    });
};