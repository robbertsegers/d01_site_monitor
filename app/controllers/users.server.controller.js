'use strict';

/**  Module dependencies.  */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    User = mongoose.model('User'),
    _ = require('lodash');

/**  Extend user's controller  */
module.exports = _.extend(
	require('./users/users.authentication.server.controller'),
	require('./users/users.authorization.server.controller'),
	require('./users/users.password.server.controller'),
	require('./users/users.profile.server.controller')
);

module.exports.list = function list(req, res) {
	User.find({}, function(err, users){
		res.status(200).json({'data': users});
	});
};

module.exports.getUser = function getUser(req, res) {
	User.find({_id: req.params.id}, function(err, users){
		res.status(200).json({'data': users});
	});
};