'use strict';

angular.module('users').controller('UsersController',
		['$scope', '$stateParams', '$location', 'Authentication', 'Users', function
		($scope, $stateParams, $location, Authentication, Users) {

			$scope.users = [];

			// Get the users list from the resourve
			Users.get().$promise.then(function(response) {
				$scope.users = response.data;
			});
	}
]);