'use strict';

//Setting up route
angular.module('statuses').config(['$stateProvider',
    function($stateProvider) {
        // statuses state routing
        $stateProvider.
        state('statuses', {
            url: '/statuses',
            templateUrl: 'modules/statuses/views/statuses.client.view.html'
        }).
        state('createStatus', {
            url: '/statuses/create',
            templateUrl: 'modules/statuses/views/create-status.client.view.html'
        });
    }
]);