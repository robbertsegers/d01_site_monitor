'use strict';

angular.module('statuses').controller('StatusesController', ['$scope', '$location', '$state', 'Statuses',
    function($scope, $location, $state, Statuses) {

        console.log('statuses controller');

        // Create new Statuses
        $scope.create = function() {
            // Create new Statuses object
            var status = new Status ({
                name: this.name,
                description: this.description
            });
            console.log(status);
            // Redirect after save
            status.$save(function(response) {
                $state.go('statuses');
                // Clear form fields
                $scope.name = '';
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Statuses
        $scope.find = function() {
            $scope.statuses = Statuses.query();
            console.log($scope.statuses);
        };
    }
]);