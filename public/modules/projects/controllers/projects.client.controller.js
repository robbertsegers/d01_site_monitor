'use strict';

// Projects controller
angular.module('projects').controller('ProjectsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Projects',
    function($scope, $stateParams, $location, Authentication, Projects) {

        console.log('projects controller');

        $scope.authentication = Authentication;
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.offset = 0;

       // Page changed handler
       $scope.pageChanged = function() {
            $scope.offset = ($scope.currentPage - 1) * $scope.pageSize;
       };

        // Create new Project
        $scope.create = function() {
            // Create new Project object
            var project = new Projects ({
                name: this.name,
                status: this.status,
                description: this.description
            });
            console.log(project);
            // Redirect after save
            project.$save(function(response) {
                $location.path('projects/' + response._id);

                // Clear form fields
                $scope.name = '';
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Project
        $scope.remove = function(project) {
            if ( project ) {
                project.$remove();
                for (var i in $scope.projects) {
                    console.log($scope.project[i]);
                    if ($scope.projects[i] === project) {
                        $scope.projects.splice(i, 1);
                    }
                }
            } else {
                $scope.project.$remove(function() {
                    $location.path('projects');
                });
            }
        };

        // Update existing Project
        $scope.update = function() {
            console.log('update project client side');
            var project = $scope.project;

            project.$update(function() {
                $location.path('projects/' + project._id);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Projects
        $scope.find = function() {
            $scope.projects = Projects.query();
        };

        // Find existing Project
        $scope.findOne = function() {
            $scope.project = Projects.get({
                projectId: $stateParams.projectId
            });
        };

        // Search for a category
        $scope.projectSearch = function(product) {
            $location.path('projects/' + product._id);
        };

    }
]);