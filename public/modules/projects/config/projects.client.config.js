'use strict';

// Configuring the Articles module
angular.module('projects').run(['Menus',
    function(Menus) {
        // Set top bar menu items
        Menus.addMenuItem('topbar', 'Projects', 'projects', 'dropdown', '/projects(/create)?');
        Menus.addSubMenuItem('topbar', 'projects', 'List Projects', 'projects');
        Menus.addSubMenuItem('topbar', 'projects', 'New Project', 'projects/create', [], [false], ['admin']);
    }
]);

/*
Menus.addSubMenuItem(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, [menuItemUIRoute], [isPublic], [roles]);
Adds a submenu item to an existing item object. This method also include couple of arguments:
menuId (Required) - Indicates the menu identifier.
rootMenuItemURL (Required) - Indicates the root menu item identifier.
menuItemTitle (Required) - A String title for the menu item.
menuItemURL (Required) - The path this menu item will link to.
menuItemUIRoute (Optional; Default: menuItemURL) - The UIRoute value, which is used to define the URL scheme where this menu item is marked as active.
isPublic (Optional; Default: menu.isPublic) - Indicates whether a menu item should be displayed only to authenticated users.
roles (Optional; Default: ['user']) - An array indicating the roles that are allowed to view this menu item.
position (Optional) - Specify the order of appearance
*/