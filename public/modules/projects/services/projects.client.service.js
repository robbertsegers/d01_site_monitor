'use strict';

//Project service used to communicate Project REST endpoints
angular.module('projects').factory('Projects', ['$resource',
    function($resource) {
        return $resource('projects/:projectId', { projectId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);